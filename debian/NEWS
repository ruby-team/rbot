rbot (0.9.14-1) unstable; urgency=low

  * New auth system:
    The basic hostmask-based auth is over, as long as basic auth levels.
    There is still a special owner account which can do everything. User
    accounts protected by password can now be created using 'user create
    <login>< password>'. It is still possible to user hostmask-based auth
    with ou without autologin. Unfortunately, there is no upgrade path
    provided by the Rbot authors, so you have to recreate all accounts
    and rights (only the owner account is preserved). More details on the
    new system can be found here:
      http://ruby-rbot.org/rbot-trac/wiki/NewAuthModule

  * New plugins:
    + alias: commands aliases
    + botsnack: replaces the hardcoded botsnack command
    + chanserv: sets user rights using Chanserv
    + debugger: debugging/profiling for rbot
    + delicious: Del.icio.us URL auto-submiter
    + dict: remote dictionnaries (includes the previous demauro plugin features)
    + factoids: replaces the hardcoded fact command
    + azgame: A-Z game
    + quiz: Quiz game
    + shiritori: Shiritori game
    + uno: UNO game
    + wheelfortune: Hangman/Wheel Of Fortune game
    + geoip: resolves the geographic locations of users
    + hl2: Half-Life 2 server status
    + modes: manages IRC rights (ncludes the previous opme plugin features)
    + nickrecover: tries to recover the rbot nick
    + reaction: automatic replies/reactions to expressions/actions in channel
    + remotectl: druby remote command execution for rbot (use it with rbot-remote)
    + ri: Ruby documentation
    + rss: RSS/RDF reader
    + salut: responds to salutations
    + script: allow running scripts from rbot (like mini plugins)
    + search (includes the previous google plugin features)
    + time: timezones info
    + translator: use remote translation services
    + twitter: Twitter status updater
    + urban: uses www.urbandictionary.com
    + usermodes: sets rbot usermodes on connect
    + wall: allow to read/write rbot site "wall" on http://ruby-rbot.org/wall/
    + youtube: YouTube info/search

 -- Marc Dequènes (Duck) <Duck@DuckCorp.org>  Thu, 02 Apr 2009 01:41:01 +0200

rbot (0.9.9-1) unstable; urgency=low

  * New configuration file:
    'conf.rbot' has been replaced by 'conf.yaml', which is more manageable,
    allowing on the fly configuration modifications using the "config"
    command (see: help config). You need to run the bot interractively
    the first time after upgrading, then a wizard will ask you questions
    to fill the necessary minimal parameters into the initial 'conf.yaml'
    file. Rbot may not guess the right directory (mostly if you use SU),
    so you're advised to specify the config directory in the command line.

  * Plugin directory has changed:
    plugins used to be in '/usr/lib/ruby/1.8/rbot/plugins', and now live
    in '/usr/share/rbot/plugins', so you need to move your custom
    system-wide plugins manually.

  * New plugins:
    + autoop
    + opme
    + quakeauth: Q auth on Quakenet network
    + roshambo: rock-paper-scissors game
    + tube: London Tube transport information

 -- Marc Dequènes (Duck) <Duck@DuckCorp.org>  Wed, 24 Aug 2005 23:58:04 +0200
